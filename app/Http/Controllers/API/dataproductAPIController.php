<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatedataproductAPIRequest;
use App\Http\Requests\API\UpdatedataproductAPIRequest;
use App\Models\dataproduct;
use App\Repositories\dataproductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class dataproductController
 * @package App\Http\Controllers\API
 */

class dataproductAPIController extends AppBaseController
{
    /** @var  dataproductRepository */
    private $dataproductRepository;

    public function __construct(dataproductRepository $dataproductRepo)
    {
        $this->dataproductRepository = $dataproductRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/dataproducts",
     *      summary="Get a listing of the dataproducts.",
     *      tags={"dataproduct"},
     *      description="Get all dataproducts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/dataproduct")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->dataproductRepository->pushCriteria(new RequestCriteria($request));
        $this->dataproductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $dataproducts = $this->dataproductRepository->all();

        return $this->sendResponse($dataproducts->toArray(), 'Dataproducts retrieved successfully');
    }

    /**
     * @param CreatedataproductAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/dataproducts",
     *      summary="Store a newly created dataproduct in storage",
     *      tags={"dataproduct"},
     *      description="Store dataproduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="dataproduct that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/dataproduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/dataproduct"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatedataproductAPIRequest $request)
    {
        $input = $request->all();

        $dataproducts = $this->dataproductRepository->create($input);

        return $this->sendResponse($dataproducts->toArray(), 'Dataproduct saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/dataproducts/{id}",
     *      summary="Display the specified dataproduct",
     *      tags={"dataproduct"},
     *      description="Get dataproduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of dataproduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/dataproduct"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var dataproduct $dataproduct */
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            return $this->sendError('Dataproduct not found');
        }

        return $this->sendResponse($dataproduct->toArray(), 'Dataproduct retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatedataproductAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/dataproducts/{id}",
     *      summary="Update the specified dataproduct in storage",
     *      tags={"dataproduct"},
     *      description="Update dataproduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of dataproduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="dataproduct that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/dataproduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/dataproduct"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatedataproductAPIRequest $request)
    {
        $input = $request->all();

        /** @var dataproduct $dataproduct */
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            return $this->sendError('Dataproduct not found');
        }

        $dataproduct = $this->dataproductRepository->update($input, $id);

        return $this->sendResponse($dataproduct->toArray(), 'dataproduct updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/dataproducts/{id}",
     *      summary="Remove the specified dataproduct from storage",
     *      tags={"dataproduct"},
     *      description="Delete dataproduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of dataproduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var dataproduct $dataproduct */
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            return $this->sendError('Dataproduct not found');
        }

        $dataproduct->delete();

        return $this->sendResponse($id, 'Dataproduct deleted successfully');
    }
}
