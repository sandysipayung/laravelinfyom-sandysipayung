<?php

namespace App\Http\Controllers;

use App\DataTables\dataproductDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatedataproductRequest;
use App\Http\Requests\UpdatedataproductRequest;
use App\Repositories\dataproductRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class dataproductController extends AppBaseController
{
    /** @var  dataproductRepository */
    private $dataproductRepository;

    public function __construct(dataproductRepository $dataproductRepo)
    {
        $this->dataproductRepository = $dataproductRepo;
    }

    /**
     * Display a listing of the dataproduct.
     *
     * @param dataproductDataTable $dataproductDataTable
     * @return Response
     */
    public function index(dataproductDataTable $dataproductDataTable)
    {
        return $dataproductDataTable->render('dataproducts.index');
    }

    /**
     * Show the form for creating a new dataproduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('dataproducts.create');
    }

    /**
     * Store a newly created dataproduct in storage.
     *
     * @param CreatedataproductRequest $request
     *
     * @return Response
     */
    public function store(CreatedataproductRequest $request)
    {
        $input = $request->all();
        
        $request->photo->store('images');
        
        $dataproduct = $this->dataproductRepository->create($input);

        Flash::success('Dataproduct saved successfully.');

        return redirect(route('dataproducts.index'));
    }

    /**
     * Display the specified dataproduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            Flash::error('Dataproduct not found');

            return redirect(route('dataproducts.index'));
        }

        return view('dataproducts.show')->with('dataproduct', $dataproduct);
    }

    /**
     * Show the form for editing the specified dataproduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            Flash::error('Dataproduct not found');

            return redirect(route('dataproducts.index'));
        }

        return view('dataproducts.edit')->with('dataproduct', $dataproduct);
    }

    /**
     * Update the specified dataproduct in storage.
     *
     * @param  int              $id
     * @param UpdatedataproductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedataproductRequest $request)
    {
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            Flash::error('Dataproduct not found');

            return redirect(route('dataproducts.index'));
        }

        $dataproduct = $this->dataproductRepository->update($request->all(), $id);

        Flash::success('Dataproduct updated successfully.');

        return redirect(route('dataproducts.index'));
    }

    /**
     * Remove the specified dataproduct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dataproduct = $this->dataproductRepository->findWithoutFail($id);

        if (empty($dataproduct)) {
            Flash::error('Dataproduct not found');

            return redirect(route('dataproducts.index'));
        }

        $this->dataproductRepository->delete($id);

        Flash::success('Dataproduct deleted successfully.');

        return redirect(route('dataproducts.index'));
    }
}
