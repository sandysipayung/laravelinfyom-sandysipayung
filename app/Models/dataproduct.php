<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="dataproduct",
 *      required={"nama_product", "deskripsi_product", "harga_product"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nama_product",
 *          description="nama_product",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deskripsi_product",
 *          description="deskripsi_product",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class dataproduct extends Model
{
    use SoftDeletes;

    public $table = 'dataproducts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_product',
        'deskripsi_product',
        'harga_product'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama_product' => 'string',
        'deskripsi_product' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_product' => 'required|min:5',
        'deskripsi_product' => 'required|min:5',
        'harga_product' => 'required'
    ];

    
}
