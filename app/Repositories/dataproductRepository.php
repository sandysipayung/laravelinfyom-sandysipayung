<?php

namespace App\Repositories;

use App\Models\dataproduct;
use InfyOm\Generator\Common\BaseRepository;

class dataproductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_product',
        'deskripsi_product',
        'harga_product'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return dataproduct::class;
    }
}
