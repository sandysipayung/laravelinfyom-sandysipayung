@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dataproduct
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dataproduct, ['route' => ['dataproducts.update', $dataproduct->id], 'method' => 'patch']) !!}

                        @include('dataproducts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection