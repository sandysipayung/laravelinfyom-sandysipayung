<!-- Nama Product Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_product', 'Nama Product:') !!}
    {!! Form::text('nama_product', null, ['class' => 'form-control']) !!}
</div>

<!-- Deskripsi Product Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('deskripsi_product', 'Deskripsi Product:') !!}
    {!! Form::textarea('deskripsi_product', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('harga_product', 'Harga Product:') !!}
    {!! Form::number('harga_product', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('dataproducts.index') !!}" class="btn btn-default">Cancel</a>
</div>
