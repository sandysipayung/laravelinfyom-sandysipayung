<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $dataproduct->id !!}</p>
</div>

<!-- Nama Product Field -->
<div class="form-group">
    {!! Form::label('nama_product', 'Nama Product:') !!}
    <p>{!! $dataproduct->nama_product !!}</p>
</div>

<!-- Deskripsi Product Field -->
<div class="form-group">
    {!! Form::label('deskripsi_product', 'Deskripsi Product:') !!}
    <p>{!! $dataproduct->deskripsi_product !!}</p>
</div>

<!-- Harga Product Field -->
<div class="form-group">
    {!! Form::label('harga_product', 'Harga Product:') !!}
    <p>{!! $dataproduct->harga_product !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $dataproduct->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $dataproduct->updated_at !!}</p>
</div>

