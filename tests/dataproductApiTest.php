<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class dataproductApiTest extends TestCase
{
    use MakedataproductTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatedataproduct()
    {
        $dataproduct = $this->fakedataproductData();
        $this->json('POST', '/api/v1/dataproducts', $dataproduct);

        $this->assertApiResponse($dataproduct);
    }

    /**
     * @test
     */
    public function testReaddataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $this->json('GET', '/api/v1/dataproducts/'.$dataproduct->id);

        $this->assertApiResponse($dataproduct->toArray());
    }

    /**
     * @test
     */
    public function testUpdatedataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $editeddataproduct = $this->fakedataproductData();

        $this->json('PUT', '/api/v1/dataproducts/'.$dataproduct->id, $editeddataproduct);

        $this->assertApiResponse($editeddataproduct);
    }

    /**
     * @test
     */
    public function testDeletedataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $this->json('DELETE', '/api/v1/dataproducts/'.$dataproduct->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/dataproducts/'.$dataproduct->id);

        $this->assertResponseStatus(404);
    }
}
