<?php

use App\Models\dataproduct;
use App\Repositories\dataproductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class dataproductRepositoryTest extends TestCase
{
    use MakedataproductTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var dataproductRepository
     */
    protected $dataproductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->dataproductRepo = App::make(dataproductRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatedataproduct()
    {
        $dataproduct = $this->fakedataproductData();
        $createddataproduct = $this->dataproductRepo->create($dataproduct);
        $createddataproduct = $createddataproduct->toArray();
        $this->assertArrayHasKey('id', $createddataproduct);
        $this->assertNotNull($createddataproduct['id'], 'Created dataproduct must have id specified');
        $this->assertNotNull(dataproduct::find($createddataproduct['id']), 'dataproduct with given id must be in DB');
        $this->assertModelData($dataproduct, $createddataproduct);
    }

    /**
     * @test read
     */
    public function testReaddataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $dbdataproduct = $this->dataproductRepo->find($dataproduct->id);
        $dbdataproduct = $dbdataproduct->toArray();
        $this->assertModelData($dataproduct->toArray(), $dbdataproduct);
    }

    /**
     * @test update
     */
    public function testUpdatedataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $fakedataproduct = $this->fakedataproductData();
        $updateddataproduct = $this->dataproductRepo->update($fakedataproduct, $dataproduct->id);
        $this->assertModelData($fakedataproduct, $updateddataproduct->toArray());
        $dbdataproduct = $this->dataproductRepo->find($dataproduct->id);
        $this->assertModelData($fakedataproduct, $dbdataproduct->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletedataproduct()
    {
        $dataproduct = $this->makedataproduct();
        $resp = $this->dataproductRepo->delete($dataproduct->id);
        $this->assertTrue($resp);
        $this->assertNull(dataproduct::find($dataproduct->id), 'dataproduct should not exist in DB');
    }
}
