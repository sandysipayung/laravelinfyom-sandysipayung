<?php

use Faker\Factory as Faker;
use App\Models\dataproduct;
use App\Repositories\dataproductRepository;

trait MakedataproductTrait
{
    /**
     * Create fake instance of dataproduct and save it in database
     *
     * @param array $dataproductFields
     * @return dataproduct
     */
    public function makedataproduct($dataproductFields = [])
    {
        /** @var dataproductRepository $dataproductRepo */
        $dataproductRepo = App::make(dataproductRepository::class);
        $theme = $this->fakedataproductData($dataproductFields);
        return $dataproductRepo->create($theme);
    }

    /**
     * Get fake instance of dataproduct
     *
     * @param array $dataproductFields
     * @return dataproduct
     */
    public function fakedataproduct($dataproductFields = [])
    {
        return new dataproduct($this->fakedataproductData($dataproductFields));
    }

    /**
     * Get fake data of dataproduct
     *
     * @param array $postFields
     * @return array
     */
    public function fakedataproductData($dataproductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama_product' => $fake->word,
            'deskripsi_product' => $fake->text,
            'harga_product' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $dataproductFields);
    }
}
